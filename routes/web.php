<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'MainController@index');

Route::get('/rules/all', 'RulesController@index');
Route::patch('/rules/store', 'RulesController@store');
Route::delete('/rules/delete/{id}', 'RulesController@destroy');
Route::patch('/rules/update/{id}', 'RulesController@update');

Route::get('/countries/all', 'CountriesController@index');
Route::post('/countries/store', 'CountriesController@store');
Route::post('/countries/update/{id}', 'CountriesController@update');
Route::delete('/countries/delete/{id}', 'CountriesController@destroy');

Route::get('/categories/all', 'CategoriesController@index');
Route::post('/categories/store', 'CategoriesController@store');
Route::post('/categories/update/{id}', 'CategoriesController@update');
Route::delete('/categories/delete/{id}', 'CategoriesController@destroy');

Route::get('/types/all', 'TypesController@index');
Route::post('/types/store', 'TypesController@store');
Route::post('/types/update/{id}', 'TypesController@update');
Route::delete('/types/delete/{id}', 'TypesController@destroy');

Route::get('/pivot/all', 'PivotController@index');
Route::post('/pivot/store', 'PivotController@store');
Route::delete('/pivot/delete/{id}', 'PivotController@destroy');
Route::post('/pivot/update/{id}', 'PivotController@update');
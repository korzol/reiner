@section('header')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Developer Test Job</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="{{ URL::to('frontend/img/favicon.ico') }}" type="image/x-icon"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts and icons -->
    <script src="{{ URL::to('dashboard/js/plugin/webfont/webfont.min.js') }}"></script>
    <script>
	WebFont.load({
	    google: {"families":["Lato:300,400,700,900"]},
	    custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['{{ URL::to('dashboard/css/fonts.min.css') }}']},
	    active: function() {
		sessionStorage.fonts = true;
	    }
	});
    </script>

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ URL::to('dashboard/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('dashboard/css/atlantis.min.css') }}">

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <!-- <link rel="stylesheet" href="../assets/css/demo.css"> -->
</head>
<body>
@endsection
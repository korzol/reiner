@yield('header')
    <div class="wrapper">
        @yield('header-panel')

        @yield('sidebar')

        <div class="main-panel" style="width:100%">
            <div class="content" style="margin-top: inherit;">
                @yield('content')
            </div>
            {{-- <footer class="footer">
                @yield('footer')
            </footer> --}}
        </div>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    <!--   Core JS Files   -->
    <!--<script src="{{ URL::to('dashboard/js/core/jquery.3.2.1.min.js') }}"></script>-->
    <script src="{{ URL::to('dashboard/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::to('dashboard/vendor/popper.js/umd/popper.min.js') }}"></script>
<!--      <script src="{{ URL::to('dashboard/js/core/popper.min.js') }}"></script>-->
    <script src="{{ URL::to('dashboard/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- jQuery UI -->
    <script src="{{ URL::to('dashboard/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
    <script src="{{ URL::to('dashboard/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>

    <!-- jQuery Scrollbar -->
    <script src="{{ URL::to('dashboard/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>

    <script src="{{ URL::to('dashboard/js/plugin/sweetalert/sweetalert.min.js') }}"></script>

    <!-- Atlantis JS -->
    <script src="{{ URL::to('dashboard/js/atlantis.min.js') }}"></script>


@yield('javascript')
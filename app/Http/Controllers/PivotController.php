<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Pivot;
use App\PivotRules;

use App\Types;
use App\Categories;
use App\Rules;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PivotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $pivots = Pivot::with(['pivotrules'])->get();
        $i = 0;

        foreach ($pivots as $pivot)
        {
            $country    = $pivot->Country()->withTrashed()->first();
            $type       = Types::where('id', $pivot->type_id)->withTrashed()->first();
            $category   = Categories::where('id', $pivot->category_id)->withTrashed()->first();

            $array[$i]['id']        = $pivot->id;
            $array[$i]['country']   = [
                                    'id'            => $country->id,
                                    'name'          => $country->name
            ];

            if ( isset($type->id) )
            {
                $array[$i]['type']      = [
                                        'id'            => $type->id,
                                        'name'          => $type->name,
                                        'country_id'    => $type->country_id,
                ];
            }

            if ( isset($category->id) )
            {
                $array[$i]['category']  = [
                                        'id'            => $category->id,
                                        'name'          => $category->name,
                                        'type_id'       => $category->type_id,
                ];
            }

            $j = 0; $newArray = [];
            foreach ($pivot->pivotrules as $rule)
            {
                $rs = Rules::where('id', $rule->rule_id)->withTrashed()->get();
                foreach ($rs as $r)
                {
                    $newArray[$j] = [
                                    'id'                => $r->id,
                                    'name'              => $r->name,
                                    'description'       => $r->description,
                    ];
                }

                $j++;
            }

            if ( isset($newArray[0]['id']) )
            {
                $array[$i]['rules']     = $newArray;
            }
            $i++;
        }

        echo response()->json($array);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): JsonResponse
    {
        $input = json_decode($request->getContent());

        $pivot = new Pivot;
        $pivot->country_id  = $input->country->id;
        if ( $input->type->id )
        {
            $pivot->type_id     = $input->type->id;
        }

        if ( $input->category->id )
        {
            $pivot->category_id = $input->category->id;
        }

        $pivot->save();

        if ( $input->rules && is_array($input->rules) )
        {
            foreach ($input->rules as $rule)
            {
                $pr[] = new PivotRules(['rule_id' => $rule->id]);
            }

            $pivot->pivotrules()->saveMany($pr);
        }


        $input->id = $pivot->id;

        return response()->json($input);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $input = json_decode($request->getContent());

        $pivot              = Pivot::find($id);
        $pivot->country_id  = $input->country->id;
        $pivot->type_id     = $input->type->id;
        $pivot->category_id = $input->category->id;
        $pivot->save();

        foreach ($input->rules as $rule)
        {
            $pr[] = new PivotRules(['rule_id' => $rule->id]);
        }

        foreach ($pivot->pivotrules as $rule)
        {
            $rule->delete();
        }

        $pivot->pivotrules()->saveMany($pr);

        $input->id = $pivot->id;

        return response()->json($input);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id): Response
    {
        $pivot = Pivot::find($id);

        $pivotRules = $pivot->pivotrules();
        foreach ($pivotRules as $rule)
        {
            $rule->delete();
        }
        $pivot->delete();

        return response('Success');
    }
}

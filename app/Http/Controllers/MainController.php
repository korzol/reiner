<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('main');
    }
}

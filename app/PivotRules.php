<?php
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PivotRules extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = ['rule_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Pivot(): BelongsTo
    {
        return $this->belongsTo('App\Pivot', 'pivot_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Rule(): BelongsTo
    {
        return $this->belongsTo('App\Rules', 'rule_id');
    }
}

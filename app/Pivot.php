<?php
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Pivot extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = ['country_id', 'type_id', 'country_id', 'rule_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pivotRules(): HasMany
    {
        return $this->hasMany('App\PivotRules', 'pivot_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Country(): HasOne
    {
        return $this->hasOne('App\Countries', 'id', 'country_id');
    }

}
